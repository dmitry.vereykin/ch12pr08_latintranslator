/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class LatinTranslator extends JFrame {
    private JButton sinister;
    private JButton dexter;
    private JButton medium;
    private JPanel panelButton;
    private JPanel panelShow;
    private JLabel translation;

    public LatinTranslator() {
        this.setTitle("Latin Translator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        sinister = new JButton("Sinister");
        dexter = new JButton("Dexter");
        medium = new JButton("Medium");

        sinister.addActionListener(new ButtonListener());
        dexter.addActionListener(new ButtonListener());
        medium.addActionListener(new ButtonListener());

        translation = new JLabel();

        panelButton = new JPanel();
        panelButton.add(sinister);
        panelButton.add(dexter);
        panelButton.add(medium);

        panelShow = new JPanel();
        panelShow.add(translation);

        this.add(panelButton, BorderLayout.SOUTH);
        this.add(panelShow, BorderLayout.NORTH);

        setSize(320, 100);
        setVisible(true);
        setLocationRelativeTo(null);
    }


    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == sinister)
                translation.setText("Left");
            else if (e.getSource() == dexter)
                translation.setText("Right");
            else if (e.getSource() == medium)
                translation.setText("Center");
        }
    }

    public static void main(String[] args) {
        new LatinTranslator();
    }

}